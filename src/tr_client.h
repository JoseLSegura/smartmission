/* -*- coding: utf-8; mode: c++ -*- */

#ifndef _TR_CLIENT_H_
#define _TR_CLIENT_H_

#include <string>
#include <set>
#include <vector>

#include "smartmission.h"

namespace smartmission {

class TRClient {
public:
  TRClient(const char* hostname, int port);
  virtual ~TRClient();

  /* Debug method */
  // const std::string& url();

  /* RPC methods */
  /* * Torrent action requests */
  void startTorrents(const std::set<id>& ids = std::set<id>()) const;
  void stopTorrents(const std::set<id>& ids = std::set<id>()) const;
  void verifyTorrents(const std::set<id>& ids = std::set<id>()) const;
  void reannounceTorrents(const std::set<id>& ids = std::set<id>()) const;

  /* * Torrent mutators */
  void torrentSet(const std::set<id>& ids, const std::string& key, int value);
  void torrentSet(const std::set<id>& ids, const std::string& key, double value);
  void torrentSet(const std::set<id>& ids, const std::string& key, bool value);
  void torrentSet(const std::set<id>& ids, const std::string& key, std::string& value);
  void torrentSet(const std::set<id>& ids, const std::string& key,
                  const std::vector& array);
  
  // void setBandwidthPriority(const std::vector<id>& ids, int priority) const;
  // void setDownloadLimit(const std::vector<id>& ids, double maxDownloadSpeed) const;
  // void setDownloadLimited(const std::vector<id>& ids, bool limited) const;
  // void setFilesWanted(const std::vector<id>& ids, const std::vector<id>& filesIds) const;
  // void setFilesUnwanted(const std::vector<id>& ids, const std::vector<id>& filesIds) const;
  // void setHonorsSessionLimits(const std::vector<id>& ids, bool sessionUploadHonored) const;
  // void setLocation(const std::vector<id>& ids, const std::string& path) const;
  // void setPeerLimit(const std::vector<id>& ids, int maxPeers) const;
  // void setPriorityHigh(const std::vector<id>& ids, const std::vector<id>& fileIds) const;

// private:
//   CURLwrapper curl;
//   Validator   validator;

};

};

#endif // _TR_CLIENT_H_
