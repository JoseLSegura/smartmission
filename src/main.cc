/* -*- coding: utf-8; mode: c++ -*- */

#include <iostream>

#include "tr_client.h"

int main() {
  smartmission::TRClient client("localhost", 9091);

  std::cout << client.url() << std::endl;
  
  return 0;
}
